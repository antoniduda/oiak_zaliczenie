
PROJ_NAME := timer
BUILD_DIR := build
SRC_DIR := src
SRCS := $(shell find $(SRC_DIR) -name '*.c' -or -name '*.s')

OBJS := $(SRCS:%=$(BUILD_DIR)/%.o)

DIR_GUARD = mkdir -p $(@D)

all: $(OBJS)
	gcc -m32 -g -Wall -o $(BUILD_DIR)/$(PROJ_NAME) $(OBJS)

$(BUILD_DIR)/%.c.o: %.c
	$(DIR_GUARD)
	gcc -c -g -m32 -Wall -o $@ $<


$(BUILD_DIR)/%.s.o: %.s
	$(DIR_GUARD)
	as -g -32 -o $@ $<

clean:
	rm -rf $(BUILD_DIR)
	rm $(PROJ_NAME)
